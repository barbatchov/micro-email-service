Email service
===========
This is a rest standalone rest service used for send emails only using feign client.

---
Setup
-------------

* For development purposes, an email service/client called djfarrelly/maildev:latest can be used. It gonna easy your life testing email sending/receiving.
Ex, docker-compose.yml:
```yml
version: "2"
services:
    image: "djfarrelly/maildev:latest"
    mem_limit: "128m"
    ports:
        - "1080:80"
        - "25:25"
```
> The email client gonna go up into the address http://localhost:1080. And the server at port 25.

* Clone this repo whatever you want;
* Run the service as usual with maven. No profiles are needed.

The server will be registered into jHipster (eureka) as "mail-service".
> The name "servicomail" will be used for feign client calls.

Calling the service
--

Example of a client
```java
import com.barbatchov.commons.dto.MailDataDTO;
// ...

@FeignClient(name = "mail-service")
public interface SendAnEmail {
    @RequestMapping(
        method = RequestMethod.POST,
        value = "/api/send",
        consumes = { MediaType.APPLICATION_JSON_VALUE },
        produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    Boolean post(MailDataDTO mailDataDTO);
}
```


> **Note:**
1. The MailDataDTO receives the files as a string.
2. They **must** have be Base64 encoded as follows.

```java
private MailDataDTO buildEmailBody() {
    MailDataDTO data = new MailDataDTO();
    Base64DataDTO template = new Base64DataDTO();
    Base64DataDTO file1 = new Base64DataDTO();
    Base64DataDTO file2 = new Base64DataDTO();
    List<Base64DataDTO> files = new ArrayList<>();
    files.add(file1);
    files.add(file2);

    data.setName("User From System");
    data.setFrom("system@barbatchov.com");
    data.setTo("user@barbatchov.com");
    data.setSubject("Testing email");
    data.setTemplate(template);
    data.setFiles(files);

    template.setData(Base64.encodeBase64String("<b>TESTE</b>".getBytes()));
    file1.setFilename("file1");
    file1.setMediaType(MediaType.TEXT_PLAIN.toString());
    file1.setData(Base64.encodeBase64String("some content 1".getBytes()));
    file2.setFilename("file2");
    file2.setMediaType(MediaType.TEXT_PLAIN.toString());
    file2.setData(Base64.encodeBase64String("some content 2".getBytes()));

    return data;
}
```
