package com.barbatchov.mail.messageproducer.controller;

import com.barbatchov.mail.commons.dto.MailDataDTO;
import com.barbatchov.mail.messageproducer.config.MsgGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api")
public class SendController {

    @Autowired
    MsgGateway gateway;

    @PostMapping(path = "/send")
    public ResponseEntity<Boolean> post(@RequestBody @Valid final MailDataDTO mailDataDTO) {
        gateway.send(mailDataDTO);
        return ResponseEntity.status(200).body(true);
    }

    @PostMapping(path = "/send-error")
    public ResponseEntity<Boolean> postError(@RequestBody final Throwable e) {
        gateway.sendError(e);
        return ResponseEntity.status(200).body(true);
    }
}
