package com.barbatchov.mail.messageproducer;

import com.barbatchov.mail.commons.Application;
import com.barbatchov.mail.commons.utils.AppRunner;
import com.barbatchov.mail.messageproducer.config.OutputMsgBinding;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.annotation.IntegrationComponentScan;

import java.io.IOException;

@IntegrationComponentScan
@EnableBinding(OutputMsgBinding.class)
public class MailMessageProducerApplication extends Application {
    public static void main(String[] args) throws IOException {
        (new AppRunner(MailMessageProducerApplication.class, args)).run();
    }
}
