package com.barbatchov.mail.messageproducer.config;

import com.barbatchov.mail.commons.dto.MailDataDTO;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import static com.barbatchov.mail.commons.Channel.SEND_MAIL_CHANNEL;
import static com.barbatchov.mail.commons.Channel.SEND_MAIL_FAILURE_CHANNEL;

@MessagingGateway(
    defaultRequestChannel = SEND_MAIL_CHANNEL,
    errorChannel = SEND_MAIL_FAILURE_CHANNEL
)
public interface MsgGateway {
    @Gateway(requestChannel = SEND_MAIL_CHANNEL)
    void send(MailDataDTO message);

    @Gateway(replyChannel = SEND_MAIL_FAILURE_CHANNEL)
    void sendError(Throwable e);
}
