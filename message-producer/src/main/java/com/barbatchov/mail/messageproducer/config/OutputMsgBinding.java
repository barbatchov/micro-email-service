package com.barbatchov.mail.messageproducer.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

import static com.barbatchov.mail.commons.Channel.SEND_MAIL_CHANNEL;
import static com.barbatchov.mail.commons.Channel.SEND_MAIL_FAILURE_CHANNEL;

public interface OutputMsgBinding {
    @Output(SEND_MAIL_CHANNEL)
    MessageChannel output();

    @Output(SEND_MAIL_FAILURE_CHANNEL)
    MessageChannel outputError();
}
