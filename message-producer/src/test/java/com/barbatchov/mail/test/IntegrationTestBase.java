package com.barbatchov.mail.test;

import com.barbatchov.mail.messageproducer.MailMessageProducerApplication;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Base64;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootConfiguration
@SpringBootTest(classes = {MailMessageProducerApplication.class})
public class IntegrationTestBase {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private Util util;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(webApplicationContext)
            .build();
        util = new Util();
    }

    public MockMvc getMockMvc() {
        return mockMvc;
    }

    public Util getUtil() {
        return util;
    }

    public class Util {
        private final ObjectMapper objectMapper;
        private final Base64.Encoder base64Encoder;

        public Util() {
            objectMapper = new ObjectMapper();
            base64Encoder = Base64.getEncoder();
        }

        public ObjectMapper getObjectMapper() {
            return objectMapper;
        }

        public <T> String objectToJsonString(T o) throws JsonProcessingException {
            return getObjectMapper().writer().writeValueAsString(o);
        }

        public Base64.Encoder getBase64Encoder() {
            return base64Encoder;
        }
    }
}
