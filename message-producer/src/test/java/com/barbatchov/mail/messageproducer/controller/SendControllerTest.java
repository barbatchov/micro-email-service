package com.barbatchov.mail.messageproducer.controller;

import com.barbatchov.mail.commons.dto.Base64DataDTO;
import com.barbatchov.mail.commons.dto.MailDataDTO;
import com.barbatchov.mail.test.IntegrationTestBase;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class SendControllerTest extends IntegrationTestBase {

    @Test
    public void shouldPostToQueue() throws Exception {
        Base64DataDTO template = new Base64DataDTO();
        template.setData(getUtil().getBase64Encoder().encodeToString("<b>teste</b>".getBytes()));

        MailDataDTO dto = new MailDataDTO();
        dto.setFrom("teste@testando.com");
        dto.setTo("remetente@testando.com");
        dto.setSubject("Assunto");
        dto.setName("Teste");
        dto.setTemplate(template);

        getMockMvc()
            .perform(
                MockMvcRequestBuilders.post("/api/send")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUtil().objectToJsonString(dto))
            )
            .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    public void postError() throws Exception {
        Exception e = new Exception("Teste");

        getMockMvc()
            .perform(
                MockMvcRequestBuilders.post("/api/send-error")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(getUtil().objectToJsonString(e))
            )
            .andExpect(MockMvcResultMatchers.status().is(200));
    }

}