package com.barbatchov.mail.messageconsumer.config;

import com.barbatchov.mail.commons.Channel;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InputMsgBinding {
    @Input(value = Channel.SEND_MAIL_CHANNEL)
    SubscribableChannel input();
}
