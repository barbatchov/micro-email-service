package com.barbatchov.mail.messageconsumer.service;

import com.barbatchov.mail.commons.Channel;
import com.barbatchov.mail.commons.dto.Base64DataDTO;
import com.barbatchov.mail.commons.dto.MailDataDTO;
import com.barbatchov.mail.messageconsumer.service.exception.CouldNotSendEmailException;
import com.barbatchov.mail.messageconsumer.service.feign.MessageProducerFeign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class SendMailService {

    private static final Logger log = LoggerFactory.getLogger(SendMailService.class);
    private static final boolean IS_MULTIPART = true;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private MessageProducerFeign messageProducerFeign;

    @StreamListener(Channel.SEND_MAIL_CHANNEL)
    public void send(MailDataDTO mailDataDTO) {
        try {
            Integer fileSize = Optional.ofNullable(mailDataDTO.getFiles()).map(List::size).orElse(0);
            String templateData = Optional.ofNullable(mailDataDTO.getTemplate()).map(Base64DataDTO::getData).orElse("-");
            log.info(
                "SENDING MAIL TO {}, {} FROM {}, SUBJECT {}, FILES {}, TEMPLATE {}",
                mailDataDTO.getName(),
                mailDataDTO.getTo(),
                mailDataDTO.getFrom(),
                mailDataDTO.getSubject(),
                fileSize,
                templateData
            );
            javaMailSender.send(generateMimeMessage(mailDataDTO));
        } catch (Exception e) {
            log.error(
                "COULD NOT SEND THE EMAIL {}, STATUS {}, {}",
                e.getMessage(),
                HttpStatus.BAD_REQUEST,
                Arrays.toString(e.getStackTrace())
            );
            messageProducerFeign.postError(e);
            throw new CouldNotSendEmailException(e);
        }
    }

    private MimeMessage generateMimeMessage(MailDataDTO mailDataDTO) throws MessagingException, UnsupportedEncodingException {
        Base64.Decoder decoderB64 = Base64.getDecoder();
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, IS_MULTIPART);

        helper.setFrom(mailDataDTO.getFrom(), mailDataDTO.getName());
        helper.setSubject(mailDataDTO.getSubject());
        helper.setTo(mailDataDTO.getTo());

        Multipart multipart = new MimeMultipart();
        MimeBodyPart textBody = new MimeBodyPart();
        textBody.setContent(new String(decoderB64.decode(mailDataDTO.getTemplate().getData())), "text/html");
        multipart.addBodyPart(textBody);

        if (!Optional.ofNullable(mailDataDTO.getFiles()).isPresent()) {
            mimeMessage.setContent(multipart);
            return mimeMessage;
        }

        for (Base64DataDTO dto : mailDataDTO.getFiles()) {
            byte[] ba = decoderB64.decode(dto.getData());
            DataSource ds = new ByteArrayDataSource(ba, dto.getMediaType());

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDataHandler(new DataHandler(ds));
            messageBodyPart.setFileName(dto.getFilename());
            multipart.addBodyPart(messageBodyPart);
        }

        mimeMessage.setContent(multipart);

        return mimeMessage;
    }
}
