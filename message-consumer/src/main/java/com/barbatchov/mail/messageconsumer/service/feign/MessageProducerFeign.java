package com.barbatchov.mail.messageconsumer.service.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "email-message-producer")
public interface MessageProducerFeign {

    @RequestMapping(
        path = "/api/send-error",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
    )
    Boolean postError(Throwable e);
}
