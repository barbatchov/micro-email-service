package com.barbatchov.mail.messageconsumer;

import com.barbatchov.mail.commons.Application;
import com.barbatchov.mail.commons.utils.AppRunner;
import com.barbatchov.mail.messageconsumer.config.InputMsgBinding;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;

import java.io.IOException;

@EnableAutoConfiguration
@EnableFeignClients
@EnableBinding(InputMsgBinding.class)
public class MailMessageConsumerApplication extends Application {
    public static void main(String[] args) throws IOException {
        (new AppRunner(MailMessageConsumerApplication.class, args)).run();
    }
}
