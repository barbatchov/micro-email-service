package com.barbatchov.mail.messageconsumer.service.exception;

public class CouldNotSendEmailException extends RuntimeException {
    private static final String ERROR = "Could not send email! Maybe Mailer Server is down!";

    public CouldNotSendEmailException(Throwable e) {
        super(ERROR, e);
    }
}
