
${AnsiColor.BRIGHT_RED} __  __ ____   ____        ____ ___  _   _ ____  _   _ __  __ _____ ____
|  \/  / ___| / ___|      / ___/ _ \| \ | / ___|| | | |  \/  | ____|  _ \
| |\/| \___ \| |  _ _____| |  | | | |  \| \___ \| | | | |\/| |  _| | |_) |
${AnsiColor.RED}| |  | |___) | |_| |_____| |__| |_| | |\  |___) | |_| | |  | | |___|  _ <
|_|  |_|____/ \____|      \____\___/|_| \_|____/ \___/|_|  |_|_____|_| \_\

${AnsiColor.YELLOW}:: BASIS 🤓 ${AnsiColor.BRIGHT_BLUE} :: Running Spring Boot ${spring-boot.version} :: ${AnsiColor.DEFAULT}

