package com.barbatchov.mail.test;

import com.barbatchov.mail.messageconsumer.MailMessageConsumerApplication;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Base64;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootConfiguration
@SpringBootTest(classes = {MailMessageConsumerApplication.class})
public class IntegrationTestBase {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;

    private Util util;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(webApplicationContext)
            .addFilters(springSecurityFilterChain)
            .build();
        util = new Util();
    }

    public MockMvc getMockMvc() {
        return mockMvc;
    }

    public Util getUtil() {
        return util;
    }

    public class Util {
        public String encodeBase64(String data) {
            return Base64.getEncoder().encodeToString(data.getBytes());
        }
    }
}
