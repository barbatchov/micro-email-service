package com.barbatchov.mail.messageconsumer.service;

import com.amazonaws.util.IOUtils;
import com.barbatchov.mail.commons.dto.Base64DataDTO;
import com.barbatchov.mail.commons.dto.MailDataDTO;
import com.barbatchov.mail.messageconsumer.service.exception.CouldNotSendEmailException;
import com.barbatchov.mail.messageconsumer.service.feign.MessageProducerFeign;
import com.barbatchov.mail.test.IntegrationTestBase;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SendMailServiceTest extends IntegrationTestBase {

    @Autowired
    SendMailService sendMailService;

    @Autowired
    JavaMailSender javaMailSender;

    @MockBean
    MessageProducerFeign messageProducerFeign;

    @Test
    public void testEnviaMultipartWithBody() throws Exception {
        MailDataDTO data = new MailDataDTO();
        Base64DataDTO template = new Base64DataDTO();
        Base64DataDTO file1 = new Base64DataDTO();
        Base64DataDTO file2 = new Base64DataDTO();
        List<Base64DataDTO> files = new ArrayList<>();
        files.add(file1);
        files.add(file2);

        data.setName("User From System");
        data.setFrom("system@barbatchov.com");
        data.setTo("user@barbatchov.com");
        data.setSubject("Testing email with 2 text files");
        data.setTemplate(template);
        data.setFiles(files);

        template.setData(getUtil().encodeBase64("<b>TEST MULTIPART TEXT ATTACHED</b>"));
        file1.setFilename("file1");
        file1.setMediaType(MediaType.TEXT_PLAIN_VALUE);
        file1.setData(getUtil().encodeBase64("some content 1"));
        file2.setFilename("file2");
        file2.setMediaType(MediaType.TEXT_PLAIN_VALUE);
        file2.setData(getUtil().encodeBase64("some content 2"));

        sendMailService.send(data);
    }

    @Test
    public void testEnviaPdf() throws Exception {
        InputStream pdf = getClass().getClassLoader().getResource("files/test.pdf").openStream();
        MailDataDTO data = new MailDataDTO();
        Base64DataDTO template = new Base64DataDTO();
        Base64DataDTO file1 = new Base64DataDTO();
        List<Base64DataDTO> files = new ArrayList<>();
        files.add(file1);

        data.setName("User From System");
        data.setFrom("system@barbatchov.com");
        data.setTo("user@barbatchov.com");
        data.setSubject("Testing email with pdf");
        data.setTemplate(template);
        data.setFiles(files);

        template.setData(getUtil().encodeBase64("<b>TEST MULTIPART PDF ATTACHED</b>"));
        file1.setFilename("test.pdf");
        file1.setMediaType(MediaType.APPLICATION_PDF_VALUE);
        file1.setData(getUtil().encodeBase64(IOUtils.toString(pdf)));

        sendMailService.send(data);
    }

    @Test
    public void testEnviaSemArquivos() throws Exception {
        MailDataDTO data = new MailDataDTO();
        Base64DataDTO template = new Base64DataDTO();

        data.setName("User From System");
        data.setFrom("system@barbatchov.com");
        data.setTo("user@barbatchov.com");
        data.setSubject("Testing email without file");
        data.setTemplate(template);

        template.setData(getUtil().encodeBase64("<b>TEST MULTIPART NO ATTACHES</b>"));
        sendMailService.send(data);

    }

    @Test(expected = CouldNotSendEmailException.class)
    public void cantSendEmailThenSendErrorToQueue() throws Exception {
        Mockito.when(javaMailSender.createMimeMessage()).thenThrow(new RuntimeException());
        MailDataDTO data = new MailDataDTO();
        Base64DataDTO template = new Base64DataDTO();

        data.setName("User From System");
        data.setFrom("system@barbatchov.com");
        data.setTo("user@barbatchov.com");
        data.setSubject("Testing email without file");
        data.setTemplate(template);

        template.setData(getUtil().encodeBase64("<b>TEST MULTIPART NO ATTACHES</b>"));
        sendMailService.send(data);
    }
}
