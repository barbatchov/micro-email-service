package com.barbatchov.mail.messageconsumer.config;

import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Profile("test")
@Configuration
public class TestConfiguration {
    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSender sender = Mockito.mock(JavaMailSenderImpl.class);
        Properties props = new Properties();
        Session session = Session.getInstance(props, new Authenticator() {});
        BDDMockito.given(sender.createMimeMessage()).willReturn(new MimeMessage(session));
        return sender;
    }
}