package com.barbatchov.mail.commons;

public class Messages {
    public static final String MAILDATA_WRONG_EMAIL_FORMAT = "Pattern.mailData.wrongEmailFormat";
    public static final String MAILDATA_FIELD_IS_NULL = "NotNull.mailData.nullFieldError";
    public static final String BASE64DATA_WRONG_FORMAT = "Pattern.base64Data.wrongDataFormat";
}