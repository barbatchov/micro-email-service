package com.barbatchov.mail.commons.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.Environment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.sql.Date;
import java.util.Optional;
import java.util.stream.Collectors;

public class AppRunner {
    private final Logger log;
    private final SpringApplication app;
    private final String[] args;

    public AppRunner(Class mainClass, String[] args) {
        this.log = LoggerFactory.getLogger(mainClass);
        this.app = new SpringApplication(mainClass);
        this.args = args;
    }

    public SpringApplication run() throws IOException {
        DefaultProfileUtil.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        ClassLoader classLoader = app.getClassLoader();
        String appinfo = read(classLoader,"appinfo.txt");
        String buildinfo = read(classLoader, "buildinfo.txt");
        String configserverinfo = read(classLoader, "configserverinfo.txt");

        log.info(
            appinfo,

            env.getProperty("spring.application.name"),
            DefaultProfileUtil.getActiveProfiles(env),
            env.getProperty("server.port"),
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port")
        );
        log.info(
            buildinfo,

            env.getProperty("revision"),
            new Date(Long.decode(env.getProperty("timestamp")))
        );

        String serverStatus = Optional.ofNullable(env.getProperty("configserver.status"))
            .orElse("Not found or not setup for this application");
        log.info(configserverinfo, serverStatus);
        return app;
    }

    private String read(ClassLoader classLoader, String s) throws IOException {
        return read(classLoader.getResourceAsStream(s));
    }

    private String read(InputStream input) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }
}
