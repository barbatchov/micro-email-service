package com.barbatchov.mail.commons.dto;

import javax.validation.constraints.Pattern;

import static com.barbatchov.mail.commons.Expressions.IS_BASE64;
import static com.barbatchov.mail.commons.Messages.BASE64DATA_WRONG_FORMAT;

public class Base64DataDTO {
    private String filename;

    private String mediaType;

    @Pattern(regexp = IS_BASE64, message = BASE64DATA_WRONG_FORMAT)
    private String data;

    public String getFilename() {
        return filename;
    }

    public Base64DataDTO setFilename(String filename) {
        this.filename = filename;
        return this;
    }

    public String getMediaType() {
        return mediaType;
    }

    public Base64DataDTO setMediaType(String mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    public String getData() {
        return data;
    }

    public Base64DataDTO setData(String data) {
        this.data = data;
        return this;
    }
}
