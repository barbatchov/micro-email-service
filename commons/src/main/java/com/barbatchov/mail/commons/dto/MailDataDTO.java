package com.barbatchov.mail.commons.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

import static com.barbatchov.mail.commons.Expressions.EMAIL;
import static com.barbatchov.mail.commons.Messages.MAILDATA_FIELD_IS_NULL;
import static com.barbatchov.mail.commons.Messages.MAILDATA_WRONG_EMAIL_FORMAT;

public class MailDataDTO {
    @NotNull(message = MAILDATA_FIELD_IS_NULL)
    @Pattern(regexp = EMAIL, message = MAILDATA_WRONG_EMAIL_FORMAT)
    private String from;

    @NotNull(message = MAILDATA_FIELD_IS_NULL)
    @Pattern(regexp = EMAIL, message = MAILDATA_WRONG_EMAIL_FORMAT)
    private String to;

    @NotNull(message = MAILDATA_FIELD_IS_NULL)
    private String subject;

    @NotNull(message = MAILDATA_FIELD_IS_NULL)
    private String name;

    @Valid
    private Base64DataDTO template;

    @Valid
    private List<Base64DataDTO> files;

    public String getFrom() {
        return from;
    }

    public MailDataDTO setFrom(String from) {
        this.from = from;
        return this;
    }

    public String getTo() {
        return to;
    }

    public MailDataDTO setTo(String to) {
        this.to = to;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public MailDataDTO setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getName() {
        return name;
    }

    public MailDataDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Base64DataDTO getTemplate() {
        return template;
    }

    public MailDataDTO setTemplate(Base64DataDTO template) {
        this.template = template;
        return this;
    }

    public List<Base64DataDTO> getFiles() {
        return files;
    }

    public MailDataDTO setFiles(List<Base64DataDTO> files) {
        this.files = files;
        return this;
    }
}
