package com.barbatchov.mail.commons;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collection;

@EnableEurekaClient
@SpringBootApplication
@PropertySource("classpath:/build.properties")
public class Application {

    @Autowired
    private Environment env;

    @PostConstruct
    public void initApplication() {
        Logger log = LoggerFactory.getLogger(this.getClass());
        Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        log.info("Running with Spring profile(s) : {}", activeProfiles);
        if (activeProfiles.contains("dev") && activeProfiles.contains("prod")) {
            log.error("You have misconfigured your application! It should not run " +
                "with both the 'dev' and 'prod' profiles at the same time.");
        }
        if (activeProfiles.contains("dev") && activeProfiles.contains("cloud")) {
            log.error("You have misconfigured your application! It should not" +
                "run with both the 'dev' and 'cloud' profiles at the same time.");
        }
    }
}
