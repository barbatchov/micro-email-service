package com.barbatchov.mail.commons;

public class Expressions {
    public static final String EMAIL = "^[\\.a-z0-9_-]+@[\\.a-z0-9_-]+[\\.a-z]+";
    public static final String IS_BASE64 = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
}
