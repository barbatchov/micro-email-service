package com.barbatchov.mail.commons;

public class Channel {
    private Channel() {}
    public static final String SEND_MAIL_CHANNEL = "send-mail-channel";
    public static final String SEND_MAIL_FAILURE_CHANNEL = "send-mail-failure-channel";
}
