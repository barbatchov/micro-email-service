<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.barbatchov.mail</groupId>
    <artifactId>send-mail</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>Send mail service</name>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.4.5.RELEASE</version>
        <relativePath/>
    </parent>

    <properties>
        <bau-comum.version>1.0.3</bau-comum.version>
        <docker-maven-plugin.version>0.4.13</docker-maven-plugin.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jacoco-maven-plugin.version>0.7.9</jacoco-maven-plugin.version>
        <build.parent>${project.build.directory}</build.parent>
        <docker.imageName/>
        <docker.imageTag/>
        <docker.registryUrl/>
        <docker.serverId/>
        <fork/>
        <jvmArguments/>
        <profile.zipkin/>
    </properties>

    <scm>
        <url>https://barbatchov@bitbucket.org/barbatchov/micro-email-service.git</url>
        <connection>scm:git:git@bitbucket.org:barbatchov/micro-email-service.git</connection>
    </scm>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>Brixton.SR7</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <profiles>
        <profile>
            <id>zipkin</id>
            <dependencies>
                <dependency>
                    <groupId>org.springframework.cloud</groupId>
                    <artifactId>spring-cloud-starter-zipkin</artifactId>
                </dependency>
            </dependencies>
            <properties>
                <profile.zipkin>,zipkin</profile.zipkin>
            </properties>
        </profile>
    </profiles>

    <build>
        <defaultGoal>spring-boot:run</defaultGoal>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.6.0</version>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>buildnumber-maven-plugin</artifactId>
                    <version>1.4</version>
                    <executions>
                        <execution>
                            <id>generate-resources</id>
                            <phase>generate-resources</phase>
                            <goals><goal>create-metadata</goal></goals>
                            <configuration>
                                <attach>true</attach>
                                <addOutputDirectoryToResources>true</addOutputDirectoryToResources>
                            </configuration>
                        </execution>
                    </executions>
                    <configuration>
                        <doCheck>false</doCheck>
                        <doUpdate>false</doUpdate>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>${docker-maven-plugin.version}</version>
                    <configuration>
                        <imageName>${docker.imageName}</imageName>
                        <serverId>${docker.serverId}</serverId>
                        <registryUrl>${docker.registryUrl}</registryUrl>
                        <dockerDirectory>src/main/docker</dockerDirectory>
                        <resources>
                            <resource>
                                <targetPath>/</targetPath>
                                <directory>${project.build.directory}</directory>
                                <include>${project.build.finalName}.jar</include>
                            </resource>
                        </resources>
                        <imageTags>
                            <imageTag>${docker.imageTag}</imageTag>
                        </imageTags>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <configuration>
                        <executable>true</executable>
                        <fork>${fork}</fork>
                        <jvmArguments>${jvmArguments}</jvmArguments>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco-maven-plugin.version}</version>
                    <executions>
                        <execution>
                            <id>pre-unit-tests</id>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                            <configuration>
                                <!-- Sets the path to the file which contains the execution data. -->
                                <destFile>${project.build.directory}/test-results/coverage/jacoco/jacoco.exec</destFile>
                            </configuration>
                        </execution>
                        <!-- Ensures that the code coverage report for unit tests is created after unit tests have been run -->
                        <execution>
                            <id>post-unit-test</id>
                            <phase>test</phase>
                            <goals>
                                <goal>report</goal>
                            </goals>
                            <configuration>
                                <dataFile>${project.build.directory}/test-results/coverage/jacoco/jacoco.exec</dataFile>
                                <outputDirectory>${project.build.directory}/test-results/coverage/jacoco</outputDirectory>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <modules>
        <module>commons</module>
        <module>message-producer</module>
        <module>message-consumer</module>
    </modules>
</project>
